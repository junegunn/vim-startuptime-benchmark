call plug#begin('~/.vim/bundle')

" My plugins
Plug 'junegunn/vim-easy-align',       { 'on': ['<Plug>(EasyAlign)', 'EasyAlign'] }
Plug 'junegunn/vim-github-dashboard', { 'on': ['GHDashboard', 'GHActivity']      }
Plug 'junegunn/vim-emoji'
Plug 'junegunn/vim-pseudocl'
Plug 'junegunn/vim-oblique'
Plug 'junegunn/vim-fnr'
Plug 'junegunn/seoul256.vim'
Plug 'junegunn/vader.vim',     { 'on': 'Vader', 'for': 'vader' }
Plug 'junegunn/vim-ruby-x',    { 'on': 'RubyX' }
Plug 'junegunn/goyo.vim',      { 'on': 'Goyo' }
Plug 'junegunn/limelight.vim', { 'on': 'Limelight' }
Plug 'junegunn/fzf'
Plug 'junegunn/vim-after-object'

" Colors
Plug 'tomasr/molokai'
Plug 'chriskempson/vim-tomorrow-theme'

" Edit
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-commentary',      { 'on': '<Plug>Commentary' }
Plug 'mbbill/undotree',           { 'on': 'UndotreeToggle'   }
Plug 'ConradIrwin/vim-bracketed-paste'
Plug 'zerowidth/vim-copy-as-rtf', { 'on': 'CopyRTF'          }

" Tmux
Plug 'tpope/vim-tbone'
Plug 'tpope/vim-dispatch'

" Browsing
Plug 'Yggdroot/indentLine', { 'on': 'IndentLinesToggle' }
Plug 'mileszs/ack.vim',     { 'on': 'Ack'               }
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle'    }
Plug 'majutsushi/tagbar',   { 'on': 'TagbarToggle'      }
Plug 'justinmk/vim-gtfo'

" Git
Plug 'tpope/vim-fugitive'
Plug 'gregsexton/gitv', { 'on': 'Gitv' }
Plug 'airblade/vim-gitgutter'

" Lang
Plug 'vim-ruby/vim-ruby'
Plug 'VimClojure',          { 'for': 'clojure' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
Plug 'kovisoft/paredit',    { 'for': 'clojure' }
Plug 'tpope/vim-rails'
Plug 'groenewege/vim-less'
Plug 'pangloss/vim-javascript'
Plug 'kchmck/vim-coffee-script'
Plug 'plasticboy/vim-markdown'
Plug 'slim-template/vim-slim'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'wting/rust.vim',        { 'for': 'rust'    }
Plug 'derekwyatt/vim-scala',  { 'for': 'scala'   }
Plug 'Keithbsmiley/investigate.vim'

call plug#end()

