let s:darwin = has('mac')

filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" My plugins
Plugin 'junegunn/vim-easy-align'
Plugin 'junegunn/vim-github-dashboard'
Plugin 'junegunn/vim-emoji'
Plugin 'junegunn/vim-pseudocl'
Plugin 'junegunn/vim-oblique'
Plugin 'junegunn/vim-fnr'
Plugin 'junegunn/seoul256.vim'
Plugin 'junegunn/vader.vim'
Plugin 'junegunn/vim-ruby-x'
Plugin 'junegunn/goyo.vim'
Plugin 'junegunn/limelight.vim'
Plugin 'junegunn/fzf'
Plugin 'junegunn/vim-after-object'

" Colors
Plugin 'tomasr/molokai'
Plugin 'chriskempson/vim-tomorrow-theme'

" Edit
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-endwise'
Plugin 'tpope/vim-commentary'
Plugin 'mbbill/undotree'
Plugin 'ConradIrwin/vim-bracketed-paste'
Plugin 'zerowidth/vim-copy-as-rtf'

" Tmux
Plugin 'tpope/vim-tbone'
Plugin 'tpope/vim-dispatch'

" Browsing
Plugin 'Yggdroot/indentLine'
Plugin 'mileszs/ack.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'majutsushi/tagbar'
Plugin 'justinmk/vim-gtfo'

" Git
Plugin 'tpope/vim-fugitive'
Plugin 'gregsexton/gitv'
Plugin 'airblade/vim-gitgutter'

" Lang
Plugin 'vim-ruby/vim-ruby'
Plugin 'VimClojure'
Plugin 'tpope/vim-fireplace'
Plugin 'kovisoft/paredit'
Plugin 'tpope/vim-rails'
Plugin 'groenewege/vim-less'
Plugin 'pangloss/vim-javascript'
Plugin 'kchmck/vim-coffee-script'
Plugin 'plasticboy/vim-markdown'
Plugin 'slim-template/vim-slim'
Plugin 'Glench/Vim-Jinja2-Syntax'
Plugin 'wting/rust.vim'
Plugin 'derekwyatt/vim-scala'
Plugin 'Keithbsmiley/investigate.vim'

call vundle#end()
filetype plugin indent on
syntax on
