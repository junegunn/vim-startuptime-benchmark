if has('vim_starting')
  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My plugins
NeoBundle 'junegunn/vim-easy-align'
NeoBundle 'junegunn/vim-github-dashboard'
NeoBundle 'junegunn/vim-emoji'
NeoBundle 'junegunn/vim-pseudocl'
NeoBundle 'junegunn/vim-oblique'
NeoBundle 'junegunn/vim-fnr'
NeoBundle 'junegunn/seoul256.vim'
NeoBundle 'junegunn/vader.vim'
NeoBundle 'junegunn/vim-ruby-x'
NeoBundle 'junegunn/goyo.vim'
NeoBundle 'junegunn/limelight.vim'
NeoBundle 'junegunn/fzf'
NeoBundle 'junegunn/vim-after-object'

" Colors
NeoBundle 'tomasr/molokai'
NeoBundle 'chriskempson/vim-tomorrow-theme'

" Edit
NeoBundle 'tpope/vim-repeat'
NeoBundle 'tpope/vim-surround'
NeoBundle 'tpope/vim-endwise'
NeoBundle 'tpope/vim-commentary'
NeoBundle 'mbbill/undotree'
NeoBundle 'ConradIrwin/vim-bracketed-paste'
NeoBundle 'zerowidth/vim-copy-as-rtf'

" Tmux
NeoBundle 'tpope/vim-tbone'
NeoBundle 'tpope/vim-dispatch'

" Browsing
NeoBundle 'Yggdroot/indentLine'
NeoBundle 'mileszs/ack.vim'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'majutsushi/tagbar'
NeoBundle 'justinmk/vim-gtfo'

" Git
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'gregsexton/gitv'
NeoBundle 'airblade/vim-gitgutter'

" Lang
NeoBundle 'vim-ruby/vim-ruby'
NeoBundle 'VimClojure'
NeoBundle 'tpope/vim-fireplace'
NeoBundle 'kovisoft/paredit'
NeoBundle 'tpope/vim-rails'
NeoBundle 'groenewege/vim-less'
NeoBundle 'pangloss/vim-javascript'
NeoBundle 'kchmck/vim-coffee-script'
NeoBundle 'plasticboy/vim-markdown'
NeoBundle 'slim-template/vim-slim'
NeoBundle 'Glench/Vim-Jinja2-Syntax'
NeoBundle 'wting/rust.vim'
NeoBundle 'derekwyatt/vim-scala'
NeoBundle 'Keithbsmiley/investigate.vim'

call neobundle#end()

" Required:
filetype plugin indent on
syntax on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
" NeoBundleCheck

