call plug#begin('~/.vim/bundle')

" My plugins
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/vim-github-dashboard'
Plug 'junegunn/vim-emoji'
Plug 'junegunn/vim-pseudocl'
Plug 'junegunn/vim-oblique'
Plug 'junegunn/vim-fnr'
Plug 'junegunn/seoul256.vim'
Plug 'junegunn/vader.vim'
Plug 'junegunn/vim-ruby-x'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/fzf'
Plug 'junegunn/vim-after-object'

" Colors
Plug 'tomasr/molokai'
Plug 'chriskempson/vim-tomorrow-theme'

" Edit
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-commentary'
Plug 'mbbill/undotree'
Plug 'ConradIrwin/vim-bracketed-paste'
Plug 'zerowidth/vim-copy-as-rtf'

" Tmux
Plug 'tpope/vim-tbone'
Plug 'tpope/vim-dispatch'

" Browsing
Plug 'Yggdroot/indentLine'
Plug 'mileszs/ack.vim'
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'justinmk/vim-gtfo'

" Git
Plug 'tpope/vim-fugitive'
Plug 'gregsexton/gitv'
Plug 'airblade/vim-gitgutter'

" Lang
Plug 'vim-ruby/vim-ruby'
Plug 'VimClojure'
Plug 'tpope/vim-fireplace'
Plug 'kovisoft/paredit'
Plug 'tpope/vim-rails'
Plug 'groenewege/vim-less'
Plug 'pangloss/vim-javascript'
Plug 'kchmck/vim-coffee-script'
Plug 'plasticboy/vim-markdown'
Plug 'slim-template/vim-slim'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'wting/rust.vim'
Plug 'derekwyatt/vim-scala'
Plug 'Keithbsmiley/investigate.vim'

call plug#end()

