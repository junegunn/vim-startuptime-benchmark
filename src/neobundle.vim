if has('vim_starting')
  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My plugins
NeoBundleLazy 'junegunn/vim-easy-align',        { 'autoload': { 'commands': 'EasyAlign', 'mappings': '<Plug>(EasyAlign)' } }
NeoBundleLazy 'junegunn/vim-github-dashboard',  { 'autoload': { 'commands': ['GHDashboard', 'GHActivity']      } }
NeoBundle 'junegunn/vim-emoji'
NeoBundle 'junegunn/vim-pseudocl'
NeoBundle 'junegunn/vim-oblique'
NeoBundle 'junegunn/vim-fnr'
NeoBundle 'junegunn/seoul256.vim'
NeoBundleLazy 'junegunn/vader.vim',      { 'autoload': { 'commands': 'Vader', 'filetypes': 'vader' } }
NeoBundleLazy 'junegunn/vim-ruby-x',     { 'autoload': { 'commands': 'RubyX' } }
NeoBundleLazy 'junegunn/goyo.vim',       { 'autoload': { 'commands': 'Goyo' } }
NeoBundleLazy 'junegunn/limelight.vim',  { 'autoload': { 'commands': 'Limelight' } }
NeoBundleLazy 'junegunn/fzf'
NeoBundle 'junegunn/vim-after-object'

" Colors
NeoBundle 'tomasr/molokai'
NeoBundle 'chriskempson/vim-tomorrow-theme'

" Edit
NeoBundle 'tpope/vim-repeat'
NeoBundle 'tpope/vim-surround'
NeoBundle 'tpope/vim-endwise'
NeoBundleLazy 'tpope/vim-commentary',       { 'autoload': { 'mappings': '<Plug>Commentary' } }
NeoBundleLazy 'mbbill/undotree',            { 'autoload': { 'commands': 'UndotreeToggle'   } }
NeoBundle 'ConradIrwin/vim-bracketed-paste'
NeoBundleLazy 'zerowidth/vim-copy-as-rtf',  { 'autoload': { 'commands': 'CopyRTF'          } }

" Tmux
NeoBundle 'tpope/vim-tbone'
NeoBundle 'tpope/vim-dispatch'

" Browsing
NeoBundleLazy 'Yggdroot/indentLine',  { 'autoload': { 'commands': 'IndentLinesToggle' } }
NeoBundleLazy 'mileszs/ack.vim',      { 'autoload': { 'commands': 'Ack'               } }
NeoBundleLazy 'scrooloose/nerdtree',  { 'autoload': { 'commands': 'NERDTreeToggle'    } }
NeoBundleLazy 'majutsushi/tagbar',    { 'autoload': { 'commands': 'TagbarToggle'      } }
NeoBundle 'justinmk/vim-gtfo'

" Git
NeoBundle 'tpope/vim-fugitive'
NeoBundleLazy 'gregsexton/gitv',  { 'autoload': { 'commands': 'Gitv' } }
NeoBundle 'airblade/vim-gitgutter'

" Lang
NeoBundle 'vim-ruby/vim-ruby'
NeoBundleLazy 'VimClojure',           { 'autoload': { 'filetypes': 'clojure' } }
NeoBundleLazy 'tpope/vim-fireplace',  { 'autoload': { 'filetypes': 'clojure' } }
NeoBundleLazy 'kovisoft/paredit',     { 'autoload': { 'filetypes': 'clojure' } }
NeoBundle 'tpope/vim-rails'
NeoBundle 'groenewege/vim-less'
NeoBundle 'pangloss/vim-javascript'
NeoBundle 'kchmck/vim-coffee-script'
NeoBundle 'plasticboy/vim-markdown'
NeoBundle 'slim-template/vim-slim'
NeoBundle 'Glench/Vim-Jinja2-Syntax'
NeoBundleLazy 'wting/rust.vim',         { 'autoload': { 'filetypes': 'rust'    } }
NeoBundleLazy 'derekwyatt/vim-scala',   { 'autoload': { 'filetypes': 'scala'   } }
NeoBundle 'Keithbsmiley/investigate.vim'

call neobundle#end()

" Required:
filetype plugin indent on
syntax on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
" NeoBundleCheck

