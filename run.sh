#!/bin/bash

cd $(dirname $BASH_SOURCE)

mkdir -p ~/.vim/autoload ~/.vim/bundle
if [ "$1" = 'init' ]; then
  curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
# curl -fLo ~/.vim/autoload/plug.vim https://raw.github.com/junegunn/vim-plug/master/plug.vim
  if [ ! -d ~/.vim/bundle/Vundle.vim ]; then
    git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
  else
    (cd ~/.vim/bundle/Vundle.vim && git pull)
  fi
  if [ ! -d ~/.vim/bundle/neobundle.vim ]; then
    git clone https://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim
  else
    (cd ~/.vim/bundle/neobundle.vim && git pull)
  fi
  vim -Nu src/plug.vim +PlugInstall +qa
  exit 0
fi

niter=100

rm -f *.log
for vpm in no-plugin pathogen vundle neobundle neobundle-all plug plug-all; do
  ln -sf src/$vpm.vim plugins.vim
  for _ in $(seq 1 $niter); do
    vim -Nu vimrc --startuptime _.log +q
    tail -1 _.log >> $vpm.log
  done
  cat $vpm.log | awk '{ sum += $1 } END { print "\"'$vpm'\" " sum / NR }'
done
rm -f *.log
